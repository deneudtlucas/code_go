from random import randint
import movelib
import copy
import numpy as np
import time as time


class Joueur():

    def __init__(self, jeu, color):
        self.jeu = jeu
        self.color = color

    def donne_coup(self):
        pass


class Humain(Joueur):
    pass


class IA(Joueur):
    pass


class Random(IA):
    def donne_coup(self, game):
        return calculate_random(game)


class MonteCarlo(IA):
    max_nb_evals = 0
    nb_coup = 0

    def __init__(self, jeu, color, nb_simulation):
        self.jeu = jeu
        self.color = color
        self.max_nb_evals = nb_simulation

    def donne_coup(self, game):
        coup = self.simulate(game)
        self.nb_coup = self.nb_coup + 1
        return coup

    def simulation(self, game, coup):
        time1 = time.time()
        nb_simulation = 0
        simulations = []
        while nb_simulation < self.max_nb_evals:
            new_game = game.copy()
            new_game.jouer(coup)
            nb_coup_simulation = self.nb_coup + 1
            while not new_game.partie_finie:
                new_game.jouer(calculate_random(new_game))
                nb_coup_simulation = nb_coup_simulation + 1

            simulations.append(new_game.score())
            nb_simulation = nb_simulation + 1

        final_score = 0
        for score in simulations:
            final_score = final_score + score

        time2 = time.time()
        return final_score / self.max_nb_evals

    def simulate(self, game):
        if game.joueur_courant.color == 1:
            best_coup = -1
            best_score = -np.infty
            for coup in game.goban.liste_coups_oks():
                score = self.simulation(game, coup)
                if best_score < score:
                    best_score = score
                    best_coup = coup
        else:
            best_coup = -1
            best_score = np.infty
            for coup in game.goban.liste_coups_oks():
                score = self.simulation(game, coup)
                if best_score > score:
                    best_score = score
                    best_coup = coup
        return best_coup


def calculate_random(game):
    found = False
    coups = game.goban.liste_coups_oks()

    if len(coups) >= 1:
        while not found:

            random = randint(0, len(coups) - 1)
            coup = coups[random]

            if coup == -1:
                return coup

            if not movelib.IsEye(coup, game.goban, game):
                if not movelib.IsSuicide(coup, game.goban, game):
                    if not movelib.IsKo(coup, game.goban, game):
                        if movelib.IsValidMove(coup, game.goban, game):
                            return coup
                        else:
                            return -1
    else:
        return -1
